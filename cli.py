from ner.linkers.falcon import FalconLinker
from ner.linkers.wikifier import WikifierLinker
from ner.common.results import LinkingResult
from ner.modelers.base import SameAsModeler

import click
from typing import List, Optional
from orkg import ORKG

registered_linkers = {}


def log_info(info: str, verbose_level, level: int):
    """
    Log information to the terminal
    :param info: the info to write
    :param verbose_level: the system verbosity level
    :param level: the min level required to print the info
    """
    if verbose_level >= level:
        print(info)


@click.command()
@click.option("-t", "--test", is_flag=True, help="Run the script on the testing system.")
@click.option("--ignore/--no-ignore", "-i/-I", default=False, help="Ignore already linked resources or overwrite them.")
@click.option("-v", "--verbose", count=True, help="How verbose the script should be -v -vv -vvv.")
@click.option("-l", "--linker", multiple=True, default=["falcon"], help="List of linkers used by the script.")
@click.option("-u", "--user", default=None, help="The username of the ORKG user to authenticate with.")
@click.option("-p", "--password", default=None, help="The password of the ORKG user to authenticate with.")
def main(test: bool, ignore: bool, verbose: int, linker: List[str], user: str, password: str):
    """
    An awesome script to link ORKG resources to other KGs out there!
    Here is a list of supported linkers: ["falcon", "wikifier"].

    Good luck out there and be aware of the monsters under the bed!
    """
    orkg = ORKG(
        host="https://sandbox.orkg.org/" if test else "https://orkg.org/",
        creds=(user, password) if user and password else None
    )
    log_info(f"++ Using this host: {orkg.host}", verbose, 1)
    log_info("Created ORKG connection object", verbose, 3)
    linkers = [registered_linkers[l]() for l in linker]
    log_info("Linkers initialized!", verbose, 2)
    modeler = SameAsModeler(orkg, ignore)
    page = 0
    while True:
        resources = orkg.resources.get(size=20, page=page).content
        log_info(f"Fetched batch ({page + 1}) of resources", verbose, 2)
        if len(resources) == 0:
            break
        linkers_tags = linker
        for resource in resources:
            if ignore and modeler.check_if_linked(resource["id"]):
                log_info(f"Resource {resource} is ignored because it is linked!", verbose, 3)
                continue
            # TODO: overwrite logic is missing here!!!
            for idx, api in enumerate(linkers):
                log_info(f"Linking '{resource['label']}' using '{linkers_tags[idx]}'", verbose, 3)
                entity: Optional[LinkingResult] = api.link(resource["label"])
                if entity is not None:
                    modeler.add_link(resource["id"], entity)
                    log_info(f"++ New link for ({resource['id']}: {resource['label']}) created!", verbose, 3)
        log_info(f"Done linking batch ({page + 1}) :)", verbose, 2)


if __name__ == '__main__':
    registered_linkers["falcon"] = FalconLinker
    registered_linkers["wikifier"] = WikifierLinker
    main()
