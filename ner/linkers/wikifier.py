from typing import Optional
from ner.common.results import LinkingResult
from ner.linkers.base import Linker
import requests


class WikifierLinker(Linker):

    def __int__(self):
        super()
        self.tag = "wikifier"

    def link(self, string: str) -> Optional[LinkingResult]:
        # Prepare the URL.
        data = {
            "text": string, "lang": "en",
            "userKey": "ggiasmwytooofydwyhcnfihrhykjur",
            "pageRankSqThreshold": "0.8", "applyPageRankSqThreshold": "true",
            "nTopDfValuesToIgnore": "200", "nWordsToIgnoreFromList": "200",
            "wikiDataClasses": "true", "wikiDataClassIds": "false",
            "support": "true", "ranges": "false", "minLinkFrequency": "2",
            "includeCosines": "false", "maxMentionEntropy": "3"
        }
        url = "http://www.wikifier.org/annotate-article"
        # Call the Wikifier and read the response.
        response = requests.post(url, data=data)
        wikifier_response = response.json()["annotations"]
        if len(wikifier_response) > 0:
            return LinkingResult(
                wikifier_response[0]["url"],
                wikifier_response[0]["title"]
            )
        else:
            return None
