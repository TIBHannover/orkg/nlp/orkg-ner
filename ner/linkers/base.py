from typing import Optional, List
from ner.common.results import LinkingResult


class Linker(object):
    """
    A base linker that other linkers have to implement to provide a unified interface.
    """
    tag: str

    def link(self, string: str) -> Optional[LinkingResult]:
        """
        Links a string to a URI
        :param string: the string to link
        :return: an optional LinkingResult with the linked resource
        """
        pass

    def links(self, string: str) -> Optional[List[LinkingResult]]:
        """
        Links a string to a set of URIs
        :param string: the string to link
        :return: an optional list of LinkingResult
        """
        pass
