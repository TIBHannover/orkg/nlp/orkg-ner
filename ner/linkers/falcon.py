from typing import Optional
from ner.linkers.base import Linker
from ner.common.results import LinkingResult
import requests


class FalconLinker(Linker):

    def __int__(self):
        super()
        self.tag = "falcon"

    def link(self, string: str) -> Optional[LinkingResult]:
        response = requests.post("https://labs.tib.eu/falcon/api", params={"mode": "short"}, json={"text": string})
        falcon_response = response.json()
        if "entities" in falcon_response and len(falcon_response["entities"]) > 0:
            return LinkingResult(
                falcon_response["entities"][0]["URI"],
                falcon_response["entities"][0]["surface form"]
            )
        else:
            return None
