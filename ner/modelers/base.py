from ner.common.results import LinkingResult

SAME_AS_RELATION = "SAME_AS"


class Modeler(object):

    def __init__(self, orkg_client, overwrite: bool):
        self.backend = orkg_client
        self.overwrite = overwrite

    def add_link(self, resource_id: str, linking_result: LinkingResult):
        pass


class SameAsModeler(Modeler):

    def __init__(self, orkg_client, overwrite: bool = False):
        super().__init__(orkg_client, overwrite)

    def add_link(self, resource_id: str, linking_result: LinkingResult):
        """
        Creates the actual link between the resource and the linked URI in the ORKG.
        :param linking_result: the linking result containing the URI
        :param resource_id: The resource we are linking
        """
        new_uri = self.backend.literals.add(label=linking_result.uri, datatype="xsd:anyURI").content["id"]
        self.backend.statements.add(subject_id=resource_id, predicate_id=SAME_AS_RELATION, object_id=new_uri)

    def check_if_linked(self, thing_id: str):
        """
        Check if a resource has a same as relation or not (aka. is linked or not)
        :param thing_id: the thing id to check
        :return: True if it is already linked, and False o/w
        """
        return len(
            self.backend.statements.get_by_subject_and_predicate(
                subject_id=thing_id,
                predicate_id=SAME_AS_RELATION
            ).content) > 0
