
class LinkingResult:
    """
    A class representing the linking result.
    """
    uri: str
    string: str

    def __init__(self, uri: str, string: str):
        self.uri = uri
        self.string = string

    def __str__(self):
        return f"{self.string} = {self.uri}"
