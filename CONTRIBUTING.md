# How To Contribute

Great to have you here with us!

This is a simple guide about how to contribute to the codebase and how to add new functionality and extend the linkers with your own.

## Code structure
The code has two main components:
* CLI
* Linkers' logic

The `cli.py` is the file that you call from the command line, and it orchestrates the whole workflow.

The logic of the linkers is located within the `ner` package.

```commandline
.
└── ner           <- linkers' logic is here
    ├── common                    <- contains the utils and common objects
    ├── linkers                   <- contains the linkers
    ├── modelers                  <- contains the modelers
    |   ....                      <- rest of the files
```

## How to start
Simply put, you just need to follow these simple steps:
* After you understand how the code is structured, you can start by adding the linker you need to the linkers package.
* Each linker should be a subclass of `Linker` and implements the methods `link` and `links`.
* In case, you require specific modeling (i.e. how the data is added to the graph). You can in a similar manner to the linker create a modeler.
* The final step would be then to update the cli file (the main entry point)

You can take inspiration from `WikifierLinker` and `SameAsModeler` classes to create your new ones.
