# ORKG EL (Entity Linking)

This repository is dedicated to enable running various NED systems on the ORKG API and directly model the detected entity into the ORKG graph.

## How to run

As simple as running a single command in the terminal (or command line).

```bash
python cli.py -v -t -l falcon
```

you can always use the `--help` option to figure out what options to use and how.

### Before you run the script

The script is a simple python script (use python 3.6 till 3.8). You will need to have the required packages beforehand.

You can install them via `pip`

```bash
pip install requirements.txt
```

It is highly recommended that you use a `venv` to make sure not to corrupt any local installations of the packages ([how to set up venv](https://uoa-eresearch.github.io/eresearch-cookbook/recipe/2014/11/26/python-virtual-env/)).

## Running in production

The script should always be used with a username and a password (acquired from the ORKG platform). Furthermore, **always test your linkers locally or using the testing system**.

## Want to add something new

Dig in right away at our [contribution guideline](CONTRIBUTING.md). We would love to check your MR and add it to our codebase.

Good luck 😉

![](https://media.giphy.com/media/12R2bKfxceemNq/giphy.gif)

## License
[MIT](./LICENSE)